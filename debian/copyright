Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: gr-hpsdr
Upstream-Contact: https://github.com/Tom-McDermott/gr-hpsdr
Source:
 https://github.com/Tom-McDermott/gr-hpsdr
Comment:
 git archive --format=tar --prefix=gr-hpsdr-2.0/  v2.0  | xz > ../gr-hpsdr_2.0.orig.tar.xz
 Debian packages by A. Maitland Bottoms <bottoms@debian.org>
 .
 Upstream Authors:
  Tom McDermott, N5EG <n5eg@tapr.org>
Copyright: 2012-2020 Thomas C. McDermott, N5EG
License: GPL-2+

Files: cmake/* CMakeLists.txt docs/* grc/* lib/* python/*
 README.md .gitignore license.txt MANIFEST.md .clang-format
Copyright: 2014-2020 Tom McDermott, N5EG <n5eg@tapr.org>
License: GPL-2+

Files: lib/HermesProxyW.cc lib/HermesProxyW.h lib/hermesWB_impl.h
 lib/hermesNB_impl.cc lib/HermesProxy.cc lib/hermesWB_impl.cc
 lib/HermesProxy.h lib/hermesNB_impl.h include/hpsdr/hermesWB.h
 include/hpsdr/hermesNB.h
Copyright: 2014-2020 Tom McDermott, N5EG <n5eg@tapr.org>
License: GPL-3+

Files: python/CMakeLists.txt python/__init__.py lib/CMakeLists.txt
 docs/CMakeLists.txt docs/doxygen/doxyxml/base.py
 docs/doxygen/doxyxml/doxyindex.py docs/doxygen/doxyxml/text.py
 docs/doxygen/doxyxml/__init__.py docs/doxygen/CMakeLists.txt
 grc/CMakeLists.txt examples/README apps/CMakeLists.txt
 include/hpsdr/CMakeLists.txt include/hpsdr/api.h
Copyright: 2003-2012 Free Software Foundation, Inc
Comment: from GNU Radio gr_modtool
License: GPL-3+

Files: debian/*
Copyright: 2015-2021 Free Software Foundation, Inc
Comment: assigned by A. Maitland Bottoms <bottoms@debian.org>
License: GPL-2+

Files: cmake/Modules/CMakeParseArgumentsCopy.cmake
Copyright: 2010 Alexander Neundorf <neundorf@kde.org>
License: Kitware-BSD
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 * Neither the names of Kitware, Inc., the Insight Software Consortium,
   nor the names of their contributors may be used to endorse or promote
   products derived from this software without specific prior written
   permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 3 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-3+
 GNU Radio is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3, or (at your option)
 any later version.
 .
 GNU Radio is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License (GPL) version 3 can be found in the file
 '/usr/share/common-licenses/LGPL-3'.
